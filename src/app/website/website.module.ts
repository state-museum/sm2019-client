import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { CarouselModule } from 'ngx-carousels'
import { AngularMaterialModule } from '../material-module'
import { CarouselComponent } from './carousel/carousel.component'
import { HomepageAboutComponent } from './homepage-about/homepage-about.component'
import { HomepageContentComponent } from './homepage-content/homepage-content.component'
import { HomepageFooterComponent } from './homepage-footer/homepage-footer.component'
import { HomepageNotificationComponent } from './homepage-notification/homepage-notification.component'
import { HomepageComponent } from './homepage/homepage.component'
import { MainHeaderComponent } from './main-header/main-header.component'
import { NavigationComponent } from './navigation/navigation.component'

@NgModule({
  declarations: [
    HomepageComponent,
    MainHeaderComponent,
    CarouselComponent,
    NavigationComponent,
    HomepageAboutComponent,
    HomepageContentComponent,
    HomepageNotificationComponent,
    HomepageFooterComponent,
  ],
  imports: [AngularMaterialModule, CarouselModule, RouterModule],
})
export class WebsiteModule {}
