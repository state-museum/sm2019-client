import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageNotificationComponent } from './homepage-notification.component';

describe('HomepageNotificationComponent', () => {
  let component: HomepageNotificationComponent;
  let fixture: ComponentFixture<HomepageNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepageNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
