import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss'],
})
export class MainHeaderComponent implements OnInit {
  todayDate = new Date(Date.now()).toString()
  date = this.todayDate
    .split(' ')
    .splice(0, 5)
    .join(' ')

  constructor() {}

  ngOnInit() {
    console.log(this.date)
  }
}
