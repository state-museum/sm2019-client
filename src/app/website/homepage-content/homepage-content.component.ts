import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-homepage-content',
  templateUrl: './homepage-content.component.html',
  styleUrls: ['./homepage-content.component.scss'],
})
export class HomepageContentComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    let i
    let divs = document.getElementsByClassName('text-regulator')
    for (i = 0; i < divs.length; i++) {
      if (divs[i].innerHTML != null) {
        divs[i].innerHTML = divs[i].innerHTML.substr(0, 100) + '...'
        console.log(divs[i].innerHTML)
      }
    }
  }
}
