import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AuthGuard } from './auth/auth.guard'
import { LoginComponent } from './auth/login/login.component'
import { SignupComponent } from './auth/signup/signup.component'
import { BookCreateComponent } from './books/book-create/book-create.component'
import { BookListComponent } from './books/book-list/book-list.component'
import { LibHomepageComponent } from './library-website/lib-homepage/lib-homepage.component'
import { HomepageComponent } from './website/homepage/homepage.component'

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'library', component: LibHomepageComponent },
  { path: 'get-books', component: BookListComponent },
  { path: 'create', component: BookCreateComponent, canActivate: [AuthGuard] },
  { path: 'edit/:bookId', component: BookCreateComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class AppRoutingModule {}
