import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { AdminDashboardModule } from './admin-dashboard/admin-dashboard.module'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { AuthInterceptor } from './auth/auth-interceptor'
import { AuthModule } from './auth/auth.module'
import { AuthService } from './auth/auth.service'
import { BooksModule } from './books/books.module'
import { BooksService } from './books/books.service'
import { HeaderComponent } from './header/header.component'
import { LibraryWebsiteModule } from './library-website/library-website.module'
import { AngularMaterialModule } from './material-module'
import { StaffsService } from './staffs/staffs.service'
import { WebsiteModule } from './website/website.module'

@NgModule({
  declarations: [AppComponent, HeaderComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    HttpClientModule,
    BooksModule,
    AuthModule,
    WebsiteModule,
    LibraryWebsiteModule,
    AdminDashboardModule,
  ],
  providers: [
    BooksService,
    AuthService,
    StaffsService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
