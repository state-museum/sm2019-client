import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-lib-content',
  templateUrl: './lib-content.component.html',
  styleUrls: ['./lib-content.component.scss'],
})
export class LibContentComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    let i
    let divs = document.getElementsByClassName('text-regulator')
    for (i = 0; i < divs.length; i++) {
      if (divs[i].innerHTML != null) {
        divs[i].innerHTML = divs[i].innerHTML.substr(0, 100) + '...'
        console.log(divs[i].innerHTML)
      }
    }
  }
}
