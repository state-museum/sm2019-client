import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LibContentComponent } from './lib-content.component';

describe('LibContentComponent', () => {
  let component: LibContentComponent;
  let fixture: ComponentFixture<LibContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LibContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
