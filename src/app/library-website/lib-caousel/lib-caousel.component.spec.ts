import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LibCaouselComponent } from './lib-caousel.component';

describe('LibCaouselComponent', () => {
  let component: LibCaouselComponent;
  let fixture: ComponentFixture<LibCaouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LibCaouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibCaouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
