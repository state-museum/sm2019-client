import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { AuthModule } from '../auth/auth.module'
import { AngularMaterialModule } from '../material-module'
import { LibCaouselComponent } from './lib-caousel/lib-caousel.component'
import { LibContentComponent } from './lib-content/lib-content.component'
import { LibFooterComponent } from './lib-footer/lib-footer.component'
import { LibHeaderComponent } from './lib-header/lib-header.component'
import { LibHomepageComponent } from './lib-homepage/lib-homepage.component'
import { LibNavigationComponent } from './lib-navigation/lib-navigation.component'
import { LibRecentBooksComponent } from './lib-recent-books/lib-recent-books.component'

@NgModule({
  declarations: [
    LibHomepageComponent,
    LibHeaderComponent,
    LibCaouselComponent,
    LibNavigationComponent,
    LibRecentBooksComponent,
    LibContentComponent,
    LibFooterComponent,
  ],
  imports: [AngularMaterialModule, RouterModule, AuthModule],
  exports: [LibFooterComponent],
})
export class LibraryWebsiteModule {}
