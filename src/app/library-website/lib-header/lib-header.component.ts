import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-lib-header',
  templateUrl: './lib-header.component.html',
  styleUrls: ['./lib-header.component.scss'],
})
export class LibHeaderComponent implements OnInit {
  todayDate = new Date(Date.now()).toString()
  date = this.todayDate
    .split(' ')
    .splice(0, 5)
    .join(' ')

  constructor() {}

  ngOnInit() {}
}
