import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LibRecentBooksComponent } from './lib-recent-books.component';

describe('LibRecentBooksComponent', () => {
  let component: LibRecentBooksComponent;
  let fixture: ComponentFixture<LibRecentBooksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LibRecentBooksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibRecentBooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
