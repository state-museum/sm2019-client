import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LibNavigationComponent } from './lib-navigation.component';

describe('LibNavigationComponent', () => {
  let component: LibNavigationComponent;
  let fixture: ComponentFixture<LibNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LibNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
