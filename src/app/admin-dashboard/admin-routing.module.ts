import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AuthGuard } from '../auth/auth.guard'
import { AdminDashboardComponent } from './admin-homepage/admin-dashboard/admin-dashboard.component'
import { AdminHomepageComponent } from './admin-homepage/admin-homepage/admin-homepage.component'
import { CreateBookComponent } from './admin-homepage/create-book/create-book.component'
import { CreateStaffComponent } from './admin-homepage/create-staff/create-staff.component'
import { GetBooksComponent } from './admin-homepage/get-books/get-books.component'
import { GetStaffsComponent } from './admin-homepage/get-staffs/get-staffs.component'
const routes: Routes = [
  {
    path: 'admin',
    component: AdminHomepageComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        component: AdminDashboardComponent,
      },
      {
        path: 'add-book',
        component: CreateBookComponent,
      },
      {
        path: 'get-books',
        component: GetBooksComponent,
      },
      {
        path: 'edit-book/:bookId',
        component: CreateBookComponent,
      },
      {
        path: 'add-staff',
        component: CreateStaffComponent,
      },
      {
        path: 'edit-staff/:staffId',
        component: CreateStaffComponent,
      },
      {
        path: 'get-staffs',
        component: GetStaffsComponent,
      },
    ],
  },
]

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)],
  providers: [AuthGuard],
})

// tslint:disable-next-line: no-unused-expression
export class AdminRoutingModule {}
