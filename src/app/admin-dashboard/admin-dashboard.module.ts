import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { AuthModule } from '../auth/auth.module'
import { BooksModule } from '../books/books.module'
import { AngularMaterialModule } from '../material-module'
import { StaffsModule } from '../staffs/staffs.module'
import { AdminDashboardComponent } from './admin-homepage/admin-dashboard/admin-dashboard.component'
import { AdminHeaderComponent } from './admin-homepage/admin-header/admin-header.component'
import { AdminHomepageComponent } from './admin-homepage/admin-homepage/admin-homepage.component'
import { CreateBookComponent } from './admin-homepage/create-book/create-book.component'
import { CreateStaffComponent } from './admin-homepage/create-staff/create-staff.component'
import { GetBooksComponent } from './admin-homepage/get-books/get-books.component'
import { AdminRoutingModule } from './admin-routing.module';
import { GetStaffsComponent } from './admin-homepage/get-staffs/get-staffs.component'

@NgModule({
  declarations: [
    AdminHeaderComponent,
    AdminHomepageComponent,
    CreateBookComponent,
    AdminDashboardComponent,
    GetBooksComponent,
    CreateStaffComponent,
    GetStaffsComponent,
  ],
  imports: [
    AngularMaterialModule,
    RouterModule,
    AuthModule,
    CommonModule,
    AdminRoutingModule,
    BooksModule,
    FormsModule,
    StaffsModule,
  ],
})
export class AdminDashboardModule {}
