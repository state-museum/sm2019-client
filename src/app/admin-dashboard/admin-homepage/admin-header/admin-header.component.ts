import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Subscription } from 'rxjs'
import { AuthService } from 'src/app/auth/auth.service'

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss'],
})
export class AdminHeaderComponent implements OnInit {
  mode: any

  userIsAuthenticated = false
  private authListenerSubs: Subscription
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.mode = 'side'
    this.userIsAuthenticated = this.authService.getIsAuth()
    this.authListenerSubs = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated
      })
  }

  onLogout() {
    this.authService.logout()
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe()
  }

  check() {
    this.router.navigate(['/admin/add-staff'])
  }
}
