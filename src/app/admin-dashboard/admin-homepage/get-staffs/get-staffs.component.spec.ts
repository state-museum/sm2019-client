import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetStaffsComponent } from './get-staffs.component';

describe('GetStaffsComponent', () => {
  let component: GetStaffsComponent;
  let fixture: ComponentFixture<GetStaffsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetStaffsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetStaffsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
