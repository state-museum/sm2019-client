import { Component, OnInit } from '@angular/core'
import { PageEvent } from '@angular/material/paginator'
import { Subscription } from 'rxjs'
import { AuthService } from '../../../auth/auth.service'
import { Book } from '../../../books/book.model'
import { BooksService } from '../../../books/books.service'

@Component({
  selector: 'app-get-books',
  templateUrl: './get-books.component.html',
  styleUrls: ['./get-books.component.scss'],
})
export class GetBooksComponent implements OnInit {
  books: Book[] = []
  isLoading = false
  totalBooks = 0
  booksPerPage = 5
  currentPage = 1
  pageSizeOptions = [1, 5, 10]
  private booksSub: Subscription
  private authStatusSub: Subscription
  userIsAuthenticated = false

  filterOptions = ['Book Name', 'Author Name', 'Publisher', 'Language', 'Subject']
  selectedCtrlName: any

  constructor(public booksService: BooksService, private authService: AuthService) {}

  ngOnInit() {
    this.selectedCtrlName = 'Book Name'

    this.isLoading = true
    this.booksService.getBooks(this.booksPerPage, 1)
    this.booksSub = this.booksService
      .getBookUpdateListener()
      .subscribe((bookData: { books: Book[]; bookCount: number }) => {
        this.isLoading = false
        this.totalBooks = bookData.bookCount
        this.books = bookData.books
      })
    this.userIsAuthenticated = this.authService.getIsAuth()
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated
      })
  }

  onChangePage(pageData: PageEvent) {
    this.isLoading = true
    this.currentPage = pageData.pageIndex + 1
    this.booksPerPage = pageData.pageSize
    this.booksService.getBooks(this.booksPerPage, this.currentPage)
  }

  onDelete(bookId: string) {
    this.isLoading = true
    this.booksService.deleteBook(bookId).subscribe(() => {
      this.booksService.getBooks(this.booksPerPage, this.currentPage)
    })
  }

  ngOnDestroy() {
    this.booksSub.unsubscribe()
    this.authStatusSub.unsubscribe()
  }
}
