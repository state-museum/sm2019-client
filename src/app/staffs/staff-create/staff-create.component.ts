import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { Staff } from "../staff.model";
import { StaffsService } from "../staffs.service";

@Component({
  selector: "app-staff-create",
  templateUrl: "./staff-create.component.html",
  styleUrls: ["./staff-create.component.scss"]
})
export class StaffCreateComponent implements OnInit {
  staffForm: FormGroup;
  private mode = "create";
  private staffId = "string";
  isloading = false;
  staff: Staff;
  imagePreview: string;
  ErrorMessageImageNotFound: string;

  mstatuses = [
    { value: "Single" },
    { value: "Married" },
    { value: "Divorced" }
  ];
  categories = [
    { key: "1", value: "GEN - General" },
    { key: "2", value: "OBC - Other Backward Class" },
    { key: "3", value: "SC - Schedule Caste" },
    { key: "4", value: "ST - Schedule Tribe" }
  ];
  gender = [
    {
      key: "1",
      value: "Male"
    },
    {
      key: "2",
      value: "Female"
    },
    {
      key: "3",
      value: "Others"
    }
  ];
  stateDistrictDataSources: Array<any> = [
    {
      name: "Jharkhand",
      districts: ["Ranchi", "Dhandbad", "Hazaribagh"]
    },
    {
      name: "Bihar",
      districts: ["Patna", "Gaya", "BiharShariff"]
    }
  ];
  empTypes = [
    { key: 1, value: "Permanent Staff" },
    { key: 2, value: "Home Guard" },
    { key: 3, value: "Temporary Staff (Daily Wages)" }
  ];
  empStatus = [
    { key: 1, value: "Active" },
    { key: 2, value: "In-Active" }
  ];

  constructor(
    public staffsService: StaffsService,
    public route: ActivatedRoute
  ) {}

  districts: Array<any>;
  changeState(item) {
    console.log("clicked");
    this.districts = this.stateDistrictDataSources.find(
      sta => sta.name == item
    ).districts;
    console.log(this.districts);
  }

  ngOnInit() {
    this.staffForm = new FormGroup({
      adhaarno: new FormControl(null, {
        validators: [Validators.required]
      }),
      empCode: new FormControl(null),
      firstName: new FormControl(null, {
        validators: [Validators.required]
      }),
      middleName: new FormControl(null),
      lastName: new FormControl(null, {
        validators: [Validators.required]
      }),
      dob: new FormControl(null, {
        validators: [Validators.required]
      }),
      guardianName: new FormControl(null, {
        validators: [Validators.required]
      }),
      identityMark: new FormControl(null, {
        validators: [Validators.required]
      }),
      gender: new FormControl(null, {
        validators: [Validators.required]
      }),
      martialStatus: new FormControl(null, {
        validators: [Validators.required]
      }),
      category: new FormControl(null, {
        validators: [Validators.required]
      }),
      homeState: new FormControl(null, {
        validators: [Validators.required]
      }),
      homeDistrict: new FormControl(null, {
        validators: [Validators.required]
      }),
      homeAddress: new FormControl(null, {
        validators: [Validators.required]
      }),
      homePincode: new FormControl(null, {
        validators: [Validators.required]
      }),
      employementType: new FormControl(null, {
        validators: [Validators.required]
      }),
      phoneNo: new FormControl(null, {
        validators: [Validators.required]
      }),
      alternatePhoneNo: new FormControl(null, {
        validators: [Validators.required]
      }),
      bloodGroup: new FormControl(null, {
        validators: [Validators.required]
      }),
      currentDesignation: new FormControl(null, {
        validators: [Validators.required]
      }),
      image: new FormControl(null, {
        validators: [Validators.required]
      }),
      employementStatus: new FormControl(null, {
        validators: [Validators.required]
      })
    });

    // this.staffForm.get("homeState").valueChanges.subscribe(item => {
    //   this.districts = item.districts;
    //   console.log(this.districts);
    //   this.staffForm.patchValue({
    //     homeDistrict: this.districts
    //   });
    // });

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has("staffId")) {
        this.mode = "edit";
        this.staffId = paramMap.get("staffId");
        this.isloading = true;
        this.staffsService.getStaff(this.staffId).subscribe(staffData => {
          this.isloading = false;
          this.staff = {
            id: staffData._id,
            adhaarno: staffData.adhaarno,
            empCode: staffData.empCode,
            firstName: staffData.firstName,
            middleName: staffData.middleName,
            lastName: staffData.lastName,
            dob: staffData.dob,
            guardianName: staffData.guardianName,
            identityMark: staffData.identityMark,
            martialStatus: staffData.martialStatus,
            gender: staffData.gender,
            category: staffData.category,
            homeState: staffData.homeState,
            homeDistrict: staffData.homeDistrict,
            homeAddress: staffData.homeAddress,
            homePincode: staffData.homePincode,
            employementType: staffData.employementType,
            phoneNo: staffData.phoneNo,
            alternatePhoneNo: staffData.alternatePhoneNo,
            bloodGroup: staffData.bloodGroup,
            currentDesignation: staffData.currentDesignation,
            staffImagePath: staffData.staffImagePath,
            employementStatus: staffData.employementStatus
          };
          this.staffForm.setValue({
            adhaarno: this.staff.adhaarno,
            empCode: this.staff.empCode,
            firstName: this.staff.firstName,
            middleName: this.staff.middleName,
            lastName: this.staff.lastName,
            dob: this.staff.dob,
            guardianName: this.staff.guardianName,
            identityMark: this.staff.identityMark,
            martialStatus: this.staff.martialStatus,
            gender: this.staff.gender,
            category: this.staff.category,
            homeState: this.staff.homeState,
            homeDistrict: this.staff.homeDistrict,
            homeAddress: this.staff.homeAddress,
            homePincode: this.staff.homePincode,
            employementType: this.staff.employementType,
            phoneNo: this.staff.phoneNo,
            alternatePhoneNo: this.staff.alternatePhoneNo,
            bloodGroup: this.staff.bloodGroup,
            currentDesignation: this.staff.currentDesignation,
            image: this.staff.staffImagePath,
            employementStatus: this.staff.employementStatus
          });
          this.imagePreview = this.staffForm.value.image;
        });
      } else {
        this.mode = "create";
        this.staffId = null;
      }
    });
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.staffForm.patchValue({ image: file });
    this.staffForm.get("image").updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
      this.ErrorMessageImageNotFound = "";
    };
    reader.readAsDataURL(file);
  }

  onSaveStaff() {
    if (this.staffForm.invalid) {
      console.log("Error in Form" + this.staffForm);
      return;
    }

    this.isloading = true;
    if (this.mode === "create") {
      console.log(this.staffForm);
      this.staffsService.addStaff(
        this.staffForm.value.adhaarno,
        this.staffForm.value.empCode,
        this.staffForm.value.firstName,
        this.staffForm.value.middleName,
        this.staffForm.value.lastName,
        this.staffForm.value.dob,
        this.staffForm.value.guardianName,
        this.staffForm.value.identityMark,
        this.staffForm.value.gender,
        this.staffForm.value.martialStatus,
        this.staffForm.value.category,
        this.staffForm.value.homeState,
        this.staffForm.value.homeDistrict,
        this.staffForm.value.homeAddress,
        this.staffForm.value.homePincode,
        this.staffForm.value.employementType,
        this.staffForm.value.phoneNo,
        this.staffForm.value.alternatePhoneNo,
        this.staffForm.value.bloodGroup,
        this.staffForm.value.currentDesignation,
        this.staffForm.value.image,
        this.staffForm.value.employementStatus
      );
    } else {
      this.staffsService.updateStaff(
        this.staffId,
        this.staffForm.value.adhaarno,
        this.staffForm.value.empCode,
        this.staffForm.value.firstName,
        this.staffForm.value.middleName,
        this.staffForm.value.lastName,
        this.staffForm.value.dob,
        this.staffForm.value.guardianName,
        this.staffForm.value.identityMark,
        this.staffForm.value.gender,
        this.staffForm.value.martialStatus,
        this.staffForm.value.category,
        this.staffForm.value.homeState,
        this.staffForm.value.homeDistrict,
        this.staffForm.value.homeAddress,
        this.staffForm.value.homePincode,
        this.staffForm.value.employementType,
        this.staffForm.value.phoneNo,
        this.staffForm.value.alternatePhoneNo,
        this.staffForm.value.bloodGroup,
        this.staffForm.value.currentDesignation,
        this.staffForm.value.image,
        this.staffForm.value.employementStatus
      );
    }
    this.staffForm.reset();
  }

  resetForm() {
    this.staffForm.reset();
  }
}
