import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { LibraryWebsiteModule } from '../library-website/library-website.module'
import { AngularMaterialModule } from '../material-module'
import { StaffCreateComponent } from './staff-create/staff-create.component'
import { StaffListComponent } from './staff-list/staff-list.component'

@NgModule({
  declarations: [StaffCreateComponent, StaffListComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    LibraryWebsiteModule,
  ],
  exports: [StaffCreateComponent, StaffListComponent],
})
export class StaffsModule {}
