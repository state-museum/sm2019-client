import { Component, OnInit } from '@angular/core'
import { PageEvent } from '@angular/material/paginator/typings'
import { Subscription } from 'rxjs'
import { AuthService } from '../../auth/auth.service'
import { Staff } from '../staff.model'
import { StaffsService } from '../staffs.service'

@Component({
  selector: 'app-staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.scss'],
})
export class StaffListComponent implements OnInit {
  private staffs: Staff[] = []
  isLoading = false
  totalStaffs = 0
  staffsPerPage = 5
  currentPage = 1
  pageSizeOptions = [1, 5, 10]
  private staffsSub: Subscription
  private authStatusSub: Subscription
  userIsAuthenticated = false

  constructor(public staffsService: StaffsService, private authService: AuthService) {}

  ngOnInit() {
    this.isLoading = true
    this.staffsService.getStaffs(this.staffsPerPage, 1)
    this.staffsSub = this.staffsService
      .getStaffUpdateListener()
      .subscribe((staffData: { staffs: Staff[]; staffCount: number }) => {
        this.isLoading = false
        this.totalStaffs = staffData.staffCount
        this.staffs = staffData.staffs
      })
    this.userIsAuthenticated = this.authService.getIsAuth()
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated
      })
  }

  onChangePage(pageData: PageEvent) {
    this.isLoading = true
    this.currentPage = pageData.pageIndex + 1
    this.staffsPerPage = pageData.pageSize
    this.staffsService.getStaffs(this.staffsPerPage, this.currentPage)
  }

  onDelete(staffId: string) {
    this.isLoading = true
    this.staffsService.deleteStaff(staffId).subscribe(() => {
      this.staffsService.getStaffs(this.staffsPerPage, this.currentPage)
    })
  }

  ngOnDestroy() {
    this.staffsSub.unsubscribe()
    this.authStatusSub.unsubscribe()
  }
}
