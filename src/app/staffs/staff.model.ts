export interface Staff {
  id: string
  adhaarno: any
  empCode: string
  firstName: string
  middleName: string
  lastName: string
  dob: string
  guardianName: string
  identityMark: string
  martialStatus: string
  gender: string
  category: string
  homeState: string
  homeDistrict: string
  homeAddress: string
  homePincode: any
  employementType: string
  phoneNo: any
  alternatePhoneNo: any
  bloodGroup: string
  currentDesignation: string
  staffImagePath: string
  employementStatus: string
}
