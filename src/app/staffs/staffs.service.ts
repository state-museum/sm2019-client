import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Subject } from 'rxjs'
import { map } from 'rxjs/operators'
import { environment } from '../../environments/environment'
import { Staff } from './staff.model'

const BACKEND_URL = environment.apiUrl + '/staff/'
console.log(BACKEND_URL)

@Injectable({
  providedIn: 'root',
})
export class StaffsService {
  private staffs: Staff[] = []
  private staffUpdated = new Subject<{ staffs: Staff[]; staffCount: number }>()

  constructor(private http: HttpClient, private router: Router) {}

  getStaffs(staffsPerPage: number, currentPage: number) {
    const queryparams = `?pageSize=${staffsPerPage}&page=${currentPage}`
    this.http
      .get<{ message: string; staffs: any; maxStaffs: number }>(
        BACKEND_URL + 'get-staffs' + queryparams
      )
      .pipe(
        map(staffData => {
          return {
            staffs: staffData.staffs.map(staff => {
              return {
                id: staff._id,
                adhaarno: staff.adhaarno,
                empCode: staff.empCode,
                firstName: staff.firstName,
                middleName: staff.middleName,
                lastName: staff.lastName,
                dob: staff.dob,
                guardianName: staff.guardianName,
                identityMark: staff.identityMark,
                martialStatus: staff.martialStatus,
                gender: staff.gender,
                category: staff.category,
                homeState: staff.homeState,
                homeDistrict: staff.homeDistrict,
                homeAddress: staff.homeAddress,
                homePincode: staff.homePincode,
                employementType: staff.employementType,
                phoneNo: staff.phoneNo,
                alternatePhoneNo: staff.alternatePhoneNo,
                bloodGroup: staff.bloodGroup,
                currentDesignation: staff.currentDesignation,
                staffImagePath: staff.staffImagePath,
                employementStatus: staff.employementStatus,
              }
            }),
            maxStaffs: staffData.maxStaffs,
          }
        })
      )
      .subscribe(transformedStaffData => {
        this.staffs = transformedStaffData.staffs
        this.staffUpdated.next({
          staffs: [...this.staffs],
          staffCount: transformedStaffData.maxStaffs,
        })
      })
  }

  getStaffUpdateListener() {
    return this.staffUpdated.asObservable()
  }

  getStaff(id: string) {
    return this.http.get<{
      _id: string
      adhaarno: any
      empCode: string
      firstName: string
      middleName: string
      lastName: string
      dob: string
      guardianName: string
      identityMark: string
      martialStatus: string
      gender: string
      category: string
      homeState: string
      homeDistrict: string
      homeAddress: string
      homePincode: any
      employementType: string
      phoneNo: any
      alternatePhoneNo: any
      bloodGroup: string
      currentDesignation: string
      staffImagePath: string
      employementStatus: string
    }>(BACKEND_URL + '/get-staff/' + id)
  }

  addStaff(
    adhaarno: any,
    empCode: string,
    firstName: string,
    middleName: string,
    lastName: string,
    dob: string,
    guardianName: string,
    identityMark: string,
    martialStatus: string,
    gender: string,
    category: string,
    homeState: string,
    homeDistrict: string,
    homeAddress: string,
    homePincode: any,
    employementType: string,
    phoneNo: any,
    alternatePhoneNo: any,
    bloodGroup: string,
    currentDesignation: string,
    image: File,
    employementStatus: string
  ) {
    const staffData = new FormData()
    staffData.append('adhaarno', adhaarno)
    staffData.append('empCode', empCode)
    staffData.append('firstName', firstName)
    staffData.append('middleName', middleName)
    staffData.append('lastName', lastName)
    staffData.append('dob', dob)
    staffData.append('guardianName', guardianName)
    staffData.append('identityMark', identityMark)
    staffData.append('gender', gender)
    staffData.append('martialStatus', martialStatus)
    staffData.append('category', category)
    staffData.append('homeState', homeState)
    staffData.append('homeDistrict', homeDistrict)
    staffData.append('homeAddress', homeAddress)
    staffData.append('homePincode', homePincode)
    staffData.append('employementType', employementType)
    staffData.append('phoneNo', phoneNo)
    staffData.append('alternatePhoneNo', alternatePhoneNo)
    staffData.append('bloodGroup', bloodGroup)
    staffData.append('currentDesignation', currentDesignation)
    staffData.append('image', image, firstName)
    staffData.append('employementStatus', employementStatus)

    this.http
      .post<{ message: string; staff: Staff }>(BACKEND_URL + '/add-staff', staffData)
      .subscribe(responseData => {
        this.router.navigate(['/admin/get-books'])
      })
  }

  updateStaff(
    id: string,
    adhaarno: any,
    empCode: string,
    firstName: string,
    middleName: string,
    lastName: string,
    dob: string,
    guardianName: string,
    identityMark: string,
    martialStatus: string,
    gender: string,
    category: string,
    homeState: string,
    homeDistrict: string,
    homeAddress: string,
    homePincode: any,
    employementType: string,
    phoneNo: any,
    alternatePhoneNo: any,
    bloodGroup: string,
    currentDesignation: string,
    image: File | string,
    employementStatus: string
  ) {
    let staffData: Staff | FormData
    if (typeof image === 'object') {
      staffData = new FormData()
      staffData.append('id', id)
      staffData.append('adhaarno', adhaarno)
      staffData.append('empCode', empCode)
      staffData.append('firstName', firstName)
      staffData.append('middleName', middleName)
      staffData.append('lastName', lastName)
      staffData.append('dob', dob)
      staffData.append('guardianName', guardianName)
      staffData.append('identityMark', identityMark)
      staffData.append('gender', gender)
      staffData.append('martialStatus', martialStatus)
      staffData.append('category', category)
      staffData.append('homeState', homeState)
      staffData.append('homeDistrict', homeDistrict)
      staffData.append('homeAddress', homeAddress)
      staffData.append('homePincode', homePincode)
      staffData.append('employementType', employementType)
      staffData.append('phoneNo', phoneNo)
      staffData.append('alternatePhoneNo', alternatePhoneNo)
      staffData.append('bloodGroup', bloodGroup)
      staffData.append('currentDesignation', currentDesignation)
      staffData.append('image', image, firstName)
      staffData.append('employementStatus', employementStatus)
    } else {
      staffData = {
        id,
        adhaarno,
        empCode,
        firstName,
        middleName,
        lastName,
        dob,
        guardianName,
        identityMark,
        martialStatus,
        gender,
        category,
        homeState,
        homeDistrict,
        homeAddress,
        homePincode,
        employementType,
        phoneNo,
        alternatePhoneNo,
        bloodGroup,
        currentDesignation,
        staffImagePath: image,
        employementStatus,
      }
    }
    this.http.put(BACKEND_URL + '/add-staff' + id, staffData).subscribe(response => {
      this.router.navigate(['/admin/get-books'])
    })
  }

  deleteStaff(staffId: string) {
    return this.http.delete(BACKEND_URL + '/get-staff/' + staffId)
  }
}
