import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { LibraryWebsiteModule } from '../library-website/library-website.module'
import { AngularMaterialModule } from '../material-module'
import { BookCreateComponent } from './book-create/book-create.component'
import { BookFilterPipe } from './book-filter.pipe'
import { BookListComponent } from './book-list/book-list.component'

@NgModule({
  declarations: [BookCreateComponent, BookListComponent, BookFilterPipe],
  imports: [
    CommonModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    LibraryWebsiteModule,
  ],
  exports: [BookCreateComponent, BookFilterPipe],
})
export class BooksModule {}
