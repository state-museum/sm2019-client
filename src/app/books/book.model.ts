export interface Book {
  id: string
  title: string
  content: string
  imagePath: string
  author: string
  isbn: any
  publication: string
  publicationYear: any
  edition: any
  volume: any
  totalPages: any
  media: string
  subject: string
  branch: string
  language: string
}
