import { Pipe, PipeTransform } from '@angular/core'
import { Book } from './book.model'

@Pipe({
  name: 'bookFilter',
})
export class BookFilterPipe implements PipeTransform {
  transform(books: Book[], searchTerm: string): Book[] {
    if (!books || !searchTerm) {
      return books
    }
    return books.filter(
      book => book.title.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1
    )
  }
}
