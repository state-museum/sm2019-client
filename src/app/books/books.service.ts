import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Subject } from 'rxjs'
import { map } from 'rxjs/operators'
import { environment } from '../../environments/environment'
import { Book } from './book.model'

const BACKEND_URL = environment.apiUrl + '/books/'

@Injectable({ providedIn: 'root' })
export class BooksService {
  private books: Book[] = []
  private bookUpdated = new Subject<{ books: Book[]; bookCount: number }>()

  constructor(private http: HttpClient, private router: Router) {}

  getBooks(booksPerPage: number, currentPage: number) {
    const queryParams = `?pageSize=${booksPerPage}&page=${currentPage}`
    this.http
      .get<{ message: string; books: any; maxBooks: number }>(BACKEND_URL + queryParams)
      .pipe(
        map(bookData => {
          return {
            books: bookData.books.map(book => {
              return {
                title: book.title,
                content: book.content,
                id: book._id,
                imagePath: book.imagePath,
                isbn: book.isbn,
                author: book.author,
                publication: book.publication,
                publicationYear: book.publicationYear,
                edition: book.edition,
                volume: book.volume,
                totalPages: book.totalPages,
                media: book.media,
                subject: book.subject,
                branch: book.branch,
                language: book.language,
              }
            }),
            maxBooks: bookData.maxBooks,
          }
        })
      )
      .subscribe(transformedBookData => {
        this.books = transformedBookData.books
        this.bookUpdated.next({
          books: [...this.books],
          bookCount: transformedBookData.maxBooks,
        })
      })
  }

  getBookUpdateListener() {
    return this.bookUpdated.asObservable()
  }

  getBook(id: string) {
    return this.http.get<{
      _id: string
      title: string
      content: string
      imagePath: string
      isbn: any
      author: string
      publication: string
      publicationYear: any
      edition: any
      volume: any
      totalPages: any
      media: string
      subject: string
      branch: string
      language: string
    }>(BACKEND_URL + id)
  }

  addBook(
    title: string,
    content: string,
    image: File,
    isbn: any,
    author: string,
    publication: string,
    publicationYear: any,
    edition: any,
    volume: any,
    totalPages: any,
    media: string,
    subject: string,
    branch: string,
    language: string
  ) {
    const bookData = new FormData()
    bookData.append('title', title)
    bookData.append('content', content)
    bookData.append('image', image, title)
    bookData.append('isbn', isbn)
    bookData.append('author', author)
    bookData.append('publication', publication)
    bookData.append('publicationYear', publicationYear)
    bookData.append('edition', edition)
    bookData.append('volume', volume)
    bookData.append('totalPages', totalPages)
    bookData.append('media', media)
    bookData.append('subject', subject)
    bookData.append('branch', branch)
    bookData.append('language', language)

    this.http
      .post<{ message: string; book: Book }>(BACKEND_URL, bookData)
      .subscribe(responseData => {
        this.router.navigate(['/admin/get-books'])
      })
  }

  updateBook(
    id: string,
    title: string,
    content: string,
    image: File | string,
    isbn: any,
    author: string,
    publication: string,
    publicationYear: any,
    edition: any,
    volume: any,
    totalPages: any,
    media: string,
    subject: string,
    branch: string,
    language: string
  ) {
    let bookData: Book | FormData
    if (typeof image === 'object') {
      bookData = new FormData()
      bookData.append('id', id)
      bookData.append('title', title)
      bookData.append('content', content)
      bookData.append('image', image, title)
      bookData.append('isbn', isbn)
      bookData.append('author', author)
      bookData.append('publication', publication)
      bookData.append('publicationYear', publicationYear)
      bookData.append('edition', edition)
      bookData.append('volumne', volume)
      bookData.append('totalPages', totalPages)
      bookData.append('media', media)
      bookData.append('subject', subject)
      bookData.append('branch', branch)
      bookData.append('language', language)
    } else {
      bookData = {
        id,
        title,
        content,
        imagePath: image,
        isbn,
        author,
        publication,
        publicationYear,
        edition,
        volume,
        totalPages,
        media,
        subject,
        branch,
        language,
      }
    }
    this.http.put(BACKEND_URL + id, bookData).subscribe(response => {
      this.router.navigate(['/admin/get-books'])
    })
  }

  deleteBook(bookId: string) {
    return this.http.delete(BACKEND_URL + bookId)
  }
}
