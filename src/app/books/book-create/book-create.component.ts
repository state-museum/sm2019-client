import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, ParamMap } from '@angular/router'
import { Book } from '../book.model'
import { BooksService } from '../books.service'
import { mimeType } from './mime-type.validator'

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.scss'],
})
export class BookCreateComponent implements OnInit {
  private mode = 'create'
  private bookId = 'string'
  book: Book
  isLoading = false
  form: FormGroup
  imageNotAvailable = '../../../assets/images/cove_not_available.png'
  imagePreview: string = this.imageNotAvailable
  ErrorMessageImageNotFound: string
  medias = [{ value: 'Book' }]
  languages = [
    { value: 'English' },
    { value: 'Hindi' },
    { value: 'Urdu' },
    { value: 'Arabic' },
    { value: 'Persian' },
    { value: 'Tamil' },
  ]
  subjects = [
    {
      key: 1,
      value: 'Computer Science, Information & General Works',
    },
    { key: 2, value: 'Philosophy & Psychology' },
    { key: 3, value: 'Religion' },
    { key: 4, value: 'Social Science' },
    { key: 5, value: 'Language' },
    { key: 6, value: 'Natural Science & Matehematics' },
    { key: 7, value: 'Technology' },
    { key: 8, value: 'The Arts, Fine & Decorative Arts' },
    { key: 7, value: 'Literature & Rhetoric' },
    { key: 10, value: 'History & Geography' },
  ]
  mediaType = [
    { key: 1, type: 'Book' },
    { key: 2, type: 'Image' },
    { key: 3, type: 'Audio' },
    { key: 4, type: 'Presentation' },
    { key: 5, type: 'Video' },
    { key: 6, type: 'Simulation' },
    { key: 7, type: 'Application' },
    { key: 8, type: 'Animation' },
  ]
  constructor(public booksService: BooksService, public route: ActivatedRoute) {}

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)],
      }),
      isbn: new FormControl(null, {
        validators: [Validators.required],
      }),
      publication: new FormControl(null, {
        validators: [Validators.required],
      }),
      publicationYear: new FormControl(null, {
        validators: [Validators.required],
      }),
      edition: new FormControl(null, {
        validators: [Validators.required],
      }),
      author: new FormControl(null, {
        validators: [Validators.required],
      }),
      volume: new FormControl(null, {
        validators: [Validators.required],
      }),
      totalPages: new FormControl(null, {
        validators: [Validators.required],
      }),
      media: new FormControl(null, {
        validators: [Validators.required],
      }),
      subject: new FormControl(null, {
        validators: [Validators.required],
      }),
      branch: new FormControl(null, {
        validators: [Validators.required],
      }),
      language: new FormControl(null, {
        validators: [Validators.required],
      }),
      content: new FormControl(null, {
        validators: [Validators.required],
      }),
      image: new FormControl(null, {
        validators: [Validators.required],
        asyncValidators: [mimeType],
      }),
    })
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('bookId')) {
        this.mode = 'edit'
        this.bookId = paramMap.get('bookId')
        this.isLoading = true
        this.booksService.getBook(this.bookId).subscribe(bookData => {
          this.isLoading = false
          this.book = {
            id: bookData._id,
            title: bookData.title,
            content: bookData.content,
            imagePath: bookData.imagePath,
            isbn: bookData.isbn,
            author: bookData.author,
            publication: bookData.publication,
            publicationYear: bookData.publicationYear,
            edition: bookData.edition,
            volume: bookData.volume,
            totalPages: bookData.totalPages,
            media: bookData.media,
            subject: bookData.subject,
            branch: bookData.branch,
            language: bookData.language,
          }

          this.form.setValue({
            title: this.book.title,
            content: this.book.content,
            image: this.book.imagePath,
            isbn: this.book.isbn,
            author: this.book.author,
            publicationYear: this.book.publicationYear,
            publication: this.book.publication,
            edition: this.book.edition,
            volume: this.book.volume,
            totalPages: this.book.totalPages,
            media: this.book.media,
            subject: this.book.subject,
            branch: this.book.branch,
            language: this.book.language,
          })
          console.log(this.form)
          this.imagePreview = this.form.value.image
        })
      } else {
        this.mode = 'create'
        this.bookId = null
      }
    })
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0]
    this.form.patchValue({ image: file })
    this.form.get('image').updateValueAndValidity()
    const reader = new FileReader()
    reader.onload = () => {
      this.imagePreview = reader.result as string
      this.ErrorMessageImageNotFound = ''
    }
    reader.readAsDataURL(file)
  }

  onSaveBook() {
    if (this.form.invalid && this.imagePreview == this.imageNotAvailable) {
      this.ErrorMessageImageNotFound = '*Please Upload Book Cover Image'
      return
    }
    this.isLoading = true
    if (this.mode === 'create') {
      this.booksService.addBook(
        this.form.value.title,
        this.form.value.content,
        this.form.value.image,
        this.form.value.isbn,
        this.form.value.author,
        this.form.value.publication,
        this.form.value.publicationYear,
        this.form.value.edition,
        this.form.value.volume,
        this.form.value.totalPages,
        this.form.value.media,
        this.form.value.subject,
        this.form.value.branch,
        this.form.value.language
      )
    } else {
      this.booksService.updateBook(
        this.bookId,
        this.form.value.title,
        this.form.value.content,
        this.form.value.image,
        this.form.value.isbn,
        this.form.value.author,
        this.form.value.publication,
        this.form.value.publicationYear,
        this.form.value.edition,
        this.form.value.volume,
        this.form.value.totalPages,
        this.form.value.media,
        this.form.value.subject,
        this.form.value.branch,
        this.form.value.language
      )
    }

    this.form.reset()
  }

  resetForm() {
    this.form.reset()
    this.imagePreview = this.imageNotAvailable
  }
}
